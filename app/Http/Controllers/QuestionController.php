<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Question;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Resources\QuestionResource;

class QuestionController extends Controller
{
     /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('JWT', ['except' => ['index','show']]);
    }

    public function index()
    {
        return QuestionResource::collection(Question::latest()->get());
    }

    public function store(Request $request)
    {
    //    auth()->user()->question()->create($request->all());
       Question::create($request->all());
       return response('Created', Response::HTTP_CREATED);
    }

    public function show(Question $question) {
        return new QuestionResource($question);
    }

    public function update(Request $request, Question $question)
    {
        Question::whereId($question->id)->update($request->all());
        return response('Updated', Response::HTTP_ACCEPTED);
    }

    public function destroy(Question $question)
    {
        $question->delete();
        return response('Deleted', Response::HTTP_CREATED);
    }
}
